import Date.MyDate;

public class Order {
    public static final int MAX_NUMBER_ORDERED = 10;
    private int qtyOrdered;
    private DigatalVideoDisc itemsOrdered[] = new DigatalVideoDisc[MAX_NUMBER_ORDERED];
    private MyDate dateOrdered;
    public static final int MAX_LIMIT_ORDERS = 5;
    private static int nOrders = 0;

    public Order() {
        if( nOrders + 1 > MAX_LIMIT_ORDERS) {
            System.out.println("Full orders");
        }
        else {
            nOrders += 1;
            System.out.println("nOrders: " + nOrders);
        }
    }
    public int getQtyOrdered() {
        return qtyOrdered;
    }
    public void setQtyOrdered(int qtyOrdered) {
        this.qtyOrdered = qtyOrdered;
    }
    public void addDigitalVideoDisc(DigatalVideoDisc disc) {
        if(qtyOrdered == MAX_NUMBER_ORDERED) {
            System.out.println("full ordered");
        }
        else {
            itemsOrdered[qtyOrdered] = disc;
            qtyOrdered += 1;
            System.out.println("Order success. Total disc " + qtyOrdered);
        }
    }
    
    public void addDigatalVideoDisc(DigatalVideoDisc[] dvdList) {
        if(qtyOrdered == MAX_NUMBER_ORDERED || qtyOrdered + dvdList.length > MAX_NUMBER_ORDERED) {
            System.out.println("full ordered");
        }
        else {
            for(int i = 0; i < dvdList.length; ++i) {
                this.addDigitalVideoDisc(dvdList[i]);
            }
        }
    }

    public void addDigatalVideoDisc(DigatalVideoDisc dvd1, DigatalVideoDisc dvd2) {
        this.addDigitalVideoDisc(dvd1);
        this.addDigitalVideoDisc(dvd2);
    }

    public void removeDigitalVideoDisc(DigatalVideoDisc disc) {
        if( qtyOrdered == 0) System.out.println("Can not remove.Total disc: " + qtyOrdered);
        else {
            for(int i = 0; i < qtyOrdered; ++i) {
                if( itemsOrdered[i] == disc) {
                    for(int j = i+1; j <qtyOrdered; ++j) {
                        itemsOrdered[j-1] = itemsOrdered[j]; 
                    }
                }
                qtyOrdered--;
                System.out.println("Remove success");
            }
        }
    }
    public float totalCost() {
        float totalCost = 0.0f;
        for( int i = 0; i < qtyOrdered; ++i) {
            totalCost += itemsOrdered[i].getCost();
        }
        return totalCost;
    }

    public void listOrder( MyDate date ) {
        date.print();
        for( int i = 0; i < qtyOrdered; ++i) {
            System.out.println(i + " " + itemsOrdered[i].getTitle() + " " +itemsOrdered[i].getCategory() + " " +itemsOrdered[i].getDirector() +" "+ itemsOrdered[i].getLength() + ": " +  itemsOrdered[i].getCost() );
        }
        System.out.println("Total cost: " + this.totalCost());
    }
 }
