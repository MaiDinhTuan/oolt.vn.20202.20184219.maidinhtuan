package hust.soict.hedspi.aims;
import hust.soict.hedspi.aims.disc.DigitalVideoDisc;
import hust.soict.hesdpi.aims.order.Order;
import hust.soict.hesdpi.aims.utils.MyDate;
public class Aims {
	public static void main(String[] args) {
        Order anOrder = new Order();
        DigitalVideoDisc d1 = new DigitalVideoDisc();
        d1.setTitle("The Lion King");
        d1.setCategory("Animation");
        d1.setCost(19.95f);
        d1.setDirector("Roger Allers");
        d1.setLength(87);
        anOrder.addDigitalVideoDisc(d1);

        DigitalVideoDisc d2 = new DigitalVideoDisc();
        d2.setTitle("Star War");
        d2.setCategory("Science Fiction");
        d2.setCost(24.95f);
        d2.setDirector("Gepgre Lucas");
        d2.setLength(124);
        anOrder.addDigitalVideoDisc(d2);

        DigitalVideoDisc d3 = new DigitalVideoDisc();
        d3.setTitle("Aladdin");
        d3.setCategory("Animation");
        d3.setCost(18.99f);
        d3.setDirector("John Musker");
        d3.setLength(90);
        anOrder.addDigitalVideoDisc(d3);

        MyDate date = new MyDate(24, 03, 2021);
        anOrder.listOrder(date);
        DigitalVideoDisc item[] = anOrder.getDiscList();
        System.out.println("\nNow find title Star War!!!");
        for(DigitalVideoDisc disc: item) {
        	if(disc != null) {
        		if(disc.search("star war")) {//find title
            		System.out.println("Found disc.");
            		break;
            	}
        	}
        }
        DigitalVideoDisc disc = anOrder.getAluckyItem();
        System.out.println("\nLucky item: ");
        System.out.println(disc.getTitle() + " " +disc.getCategory() + " " +disc.getDirector() +" "+ disc.getLength() + ": " +  disc.getCost() );
        System.out.println("\nFinal cost: ");
        anOrder.listOrder(date);
    }
}
