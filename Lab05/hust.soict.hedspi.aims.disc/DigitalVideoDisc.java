package hust.soict.hedspi.aims.disc;

import java.util.StringTokenizer;

public class DigitalVideoDisc {
	private String title;
    private String category;
    private String director;
    private int length;
    private float cost;
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public String getDirector() {
        return director;
    }
    public void setDirector(String director) {
        this.director = director;
    }
    public int getLength() {
        return length;
    }
    public boolean setLength(int length) {
        if(length > 0) {
            this.length = length;
            return true;
        }
        else return false;
    }
    public float getCost() {
        return cost;
    }
    public void setCost(float cost) {
        this.cost = cost;
    }

    public DigitalVideoDisc() {
        this.title = "noname";
        this.category = "unknown";
        this.director = "unknown";
        this.length = 0;
        this.cost = 0.0f;
    }
    public DigitalVideoDisc(String title) {
        this.title = title;
    }
    public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
        this.title = title;
        this.category = category;
        this.director = director;
        this.length = length;
        this.cost = cost;
    }
    
    public boolean search(String title) {
    	title = title.toLowerCase();
    	String source = this.title.toLowerCase();
    	StringTokenizer token = new StringTokenizer(title);
    	String arr[];
    	arr = new String[30];
    	int i = 0;
    	int count = 0;
    	while(token.hasMoreTokens()) {
    		arr[i++] = token.nextToken();
    	}
    	for(int j = 0; j < i; ++j) {
    		if(source.contains(arr[j])) ++count;
    	}
    	if(count == i) return true;
    	else return false;
    }
}
