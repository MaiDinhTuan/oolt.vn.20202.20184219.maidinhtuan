
import java.util.Scanner;
import java.util.StringTokenizer;
public class MyDate {
    private int day;
    private int month;
    private int year;
    
    public enum Month {
        January(1),
        February(2),
        March(3),
        April(4),
        May(5),
        June(6),
        July(7),
        August(8),
        September(9),
        October(10),
        November(11),
        December(12);

        private final int value;
        
        //contrustor in enum
        private Month(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }
    //constructor
    public MyDate() {
        this.day = 1;
        this.month = 1;
        this.year = 2000;
    }

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public MyDate(String date) {
        StringTokenizer tk = new StringTokenizer(date);
        String s = tk.nextToken();
        for( Month m : Month.values()) {
            if(m.name().equals(s)) {
                this.month = m.getValue();
            }
        }

        this.day = Integer.parseInt(tk.nextToken());
        this.year = Integer.parseInt(tk.nextToken());
    }

    //get and set
    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        if(month == 2 && day > 0 && day <= 28 )
            this.day = day;
        else if( month != 2 && day > 0 && day <= 31)
            this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        if( month > 0 && month <= 12)
            this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        if( year > 0 )
            this.year = year;
    }

    public void accept() {
        Scanner obj = new Scanner(System.in);
        System.out.println("Enter date");
        String date = obj.nextLine();
        StringTokenizer tk = new StringTokenizer(date);
        String s = tk.nextToken();
        for( Month m : Month.values()) {
            if(m.name().equals(s)) {
                this.month = m.getValue();
            }
        }

        this.day = Integer.parseInt(tk.nextToken());
        this.year = Integer.parseInt(tk.nextToken());
    }

    public void print() {
        System.out.println("Current Date: " + this.day + " " + this.month + " "+ this.year);
    }
}