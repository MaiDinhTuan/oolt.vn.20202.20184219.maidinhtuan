public class Order {
    public static final int MAX_NUMBER_ORDERED = 10;
    private int qtyOrdered;
    private DigatalVideoDisc itemsOrdered[] = new DigatalVideoDisc[MAX_NUMBER_ORDERED];
    public int getQtyOrdered() {
        return qtyOrdered;
    }
    public void setQtyOrdered(int qtyOrdered) {
        this.qtyOrdered = qtyOrdered;
    }
    public void addDigitalVideoDisc(DigatalVideoDisc disc) {
        if(qtyOrdered == MAX_NUMBER_ORDERED) {
            System.out.println("full ordered");
        }
        else {
            itemsOrdered[qtyOrdered] = disc;
            qtyOrdered += 1;
            System.out.println("Order success. Total disc " + qtyOrdered);
        }

    }
    public void removeDigitalVideoDisc(DigatalVideoDisc disc) {
        if( qtyOrdered == 0) System.out.println("Can not remove.Total disc: " + qtyOrdered);
        else {
            for(int i = 0; i < qtyOrdered; ++i) {
                if( itemsOrdered[i] == disc) {
                    for(int j = i+1; j <qtyOrdered; ++j) {
                        itemsOrdered[j-1] = itemsOrdered[j]; 
                    }
                }
                qtyOrdered--;
                System.out.println("Remove success");
            }
        }
    }
    public float totalCost() {
        float totalCost = 0.0f;
        for( int i = 0; i < qtyOrdered; ++i) {
            totalCost += itemsOrdered[i].getCost();
        }
        return totalCost;
    }
 }
