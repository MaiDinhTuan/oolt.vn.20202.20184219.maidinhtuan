public class Aims {
     public static void main(String[] args) {
        Order anOrder = new Order();
        DigatalVideoDisc d1 = new DigatalVideoDisc();
        d1.setCategory("Animation");
        d1.setCost(19.95f);
        d1.setDirector("Roger Allers");
        d1.setLength(87);
        anOrder.addDigitalVideoDisc(d1);

        DigatalVideoDisc d2 = new DigatalVideoDisc();
        d2.setCategory("Science Fiction");
        d2.setCost(24.95f);
        d2.setDirector("Gepgre Lucas");
        d2.setLength(124);
        anOrder.addDigitalVideoDisc(d2);

        DigatalVideoDisc d3 = new DigatalVideoDisc();
        d3.setCategory("Animation");
        d3.setCost(18.99f);
        d3.setDirector("John Musker");
        d3.setLength(90);
        anOrder.addDigitalVideoDisc(d3);

        anOrder.removeDigitalVideoDisc(d2);
    }
}
